exports.userWins = function(voxaEvent) {
  return voxaEvent.model.userWins;
};

exports.alexaWins = function(voxaEvent) {
  return voxaEvent.model.alexaWins;
};

exports.winner = function(voxaEvent) {
  return voxaEvent.model.winner;
};

exports.wins = function(voxaEvent) {
  return voxaEvent.model.wins;
};
