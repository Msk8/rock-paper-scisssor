function register(voxaApp) {
  voxaApp.onIntent("LaunchIntent", () => {
    return {
      flow: "continue",
      reply: "Welcome",
      to: "askHowManyWins",
    };
  });

  voxaApp.onState("askHowManyWins", () => {
    return {
      flow: "yield",
      reply: "AskHowManyWins",
      to: "getHowManyWins",
    };
  });

  voxaApp.onState("getHowManyWins", voxaEvent => {
    if (voxaEvent.intent.name === "MaxWinsIntent") {
      voxaEvent.model.wins = voxaEvent.intent.params.wins;
      voxaEvent.model.userWins = 0;
      voxaEvent.model.alexaWins = 0;
      voxaEvent.model.winner = false;

      if (voxaEvent.model.wins <= 10) {
        return {
          flow: "continue",
          reply: "StartGame",
          to: "askUserChoice",
        };
      }

      if (voxaEvent.model.wins > 10) {
        return {
          flow: "yield",
          reply: "BiggerThanTen",
          to: "shouldContinueLongGame",
        };
      }
    }
  });

  voxaApp.onState("shouldContinueLongGame", voxaEvent => {
    if (voxaEvent.intent.name === "YesIntent") {
      return {
        flow: "continue",
        reply: "StartGame",
        to: "askUserChoice",
      };
    }

    if (voxaEvent.intent.name === "NoIntent") {
      return {
        flow: "continue",
        reply: "Welcome",
        to: "askHowManyWins",
      };
    }
  });

  const CHOICES = ["rock", "paper", "scissors"];

  voxaApp.onState("askUserChoice", voxaEvent => {
    const userWon =
      parseInt(voxaEvent.model.userWins) >= parseInt(voxaEvent.model.wins);
    const alexaWon =
      parseInt(voxaEvent.model.alexaWins) >= parseInt(voxaEvent.model.wins);

    if (userWon) {
      voxaEvent.model.winner = true;
      return {
        flow: "continue",
        reply: "UserWinsTheGame",
        to: "askIfStartANewGame",
      };
    }

    if (alexaWon) {
      voxaEvent.model.winner = true;
      return {
        flow: "continue",
        reply: "AlexaWinsTheGame",
        to: "askIfStartANewGame",
      };
    }

    const min = 0;
    const max = CHOICES.length - 1;
    voxaEvent.model.userChoice = undefined;
    voxaEvent.model.alexaChoice =
      Math.floor(Math.random() * (max - min + 1)) + min;

    return {
      flow: "yield",
      reply: "AskUserChoice",
      to: "getUserChoice",
    };
  });

  voxaApp.onState("getUserChoice", voxaEvent => {
    if (voxaEvent.intent.name === "RockIntent") {
      voxaEvent.model.userChoice = "rock";
    } else if (voxaEvent.intent.name === "PaperIntent") {
      voxaEvent.model.userChoice = "paper";
    } else if (voxaEvent.intent.name === "ScissorsIntent") {
      voxaEvent.model.userChoice = "scissors";
    }

    if (voxaEvent.model.userChoice) {
      return {
        flow: "continue",
        to: "processWinner",
      };
    }
  });

  voxaApp.onState("processWinner", voxaEvent => {
    const alexaChoice = CHOICES[voxaEvent.model.alexaChoice];
    const { userChoice } = voxaEvent.model;
    let reply = "TiedResult";

    if (alexaChoice === userChoice) {
      return {
        flow: "continue",
        reply,
        to: "askUserChoice",
      };
    }

    if (alexaChoice === "rock") {
      if (userChoice === "paper") {
        voxaEvent.model.userWins += 1;
        reply = "UserWins";
      }

      if (userChoice === "scissors") {
        voxaEvent.model.alexaWins += 1;
        reply = "AlexaWins";
      }
    }

    if (alexaChoice === "paper") {
      if (userChoice === "scissors") {
        voxaEvent.model.userWins += 1;
        reply = "UserWins";
      }

      if (userChoice === "rock") {
        voxaEvent.model.alexaWins += 1;
        reply = "AlexaWins";
      }
    }

    if (alexaChoice === "scissors") {
      if (userChoice === "rock") {
        voxaEvent.model.userWins += 1;
        reply = "UserWins";
      }

      if (userChoice === "paper") {
        voxaEvent.model.alexaWins += 1;
        reply = "AlexaWins";
      }
    }

    return {
      flow: "continue",
      reply,
      to: "askUserChoice",
    };
  });

  voxaApp.onState("askIfStartANewGame", () => {
    return {
      flow: "yield",
      reply: "AskIfStartANewGame",
      to: "shouldStartANewGame",
    };
  });

  voxaApp.onState("shouldStartANewGame", voxaEvent => {
    if (voxaEvent.intent.name === "YesIntent") {
      return {
        flow: "continue",
        reply: "RestartGame",
        to: "askHowManyWins",
      };
    }

    if (voxaEvent.intent.name === "NoIntent") {
      if (voxaEvent.model.winner === false) {
        return {
          flow: "yield",
          reply: "AskUserChoice",
          to: "getUserChoice",
        };
      }

      if (voxaEvent.model.winner === true) {
        return {
          flow: "terminate",
          reply: "Bye",
        };
      }
    }
  });

  voxaApp.onIntent("CancelIntent", () => {
    return {
      flow: "terminate",
      reply: "Bye",
    };
  });

  voxaApp.onIntent("StopIntent", () => {
    return {
      flow: "terminate",
      reply: "Bye",
    };
  });

  voxaApp.onIntent("ActualScoreIntent", voxaEvent => {
    if (voxaEvent.model.wins == undefined) {
      return {
        flow: "yield",
        reply: "NoScore",
        to: "shouldStartANewGame",
      };
    } else {
      return {
        flow: "yield",
        reply: "ActualScore",
        to: "ShouldContinueGame",
      };
    }
  });

  voxaApp.onState("ShouldContinueGame", voxaEvent => {
    if (voxaEvent.intent.name === "YesIntent") {
      return {
        flow: "yield",
        reply: "AskUserChoice",
        to: "getUserChoice",
      };
    }

    if (voxaEvent.intent.name === "NoIntent") {
      return {
        flow: "terminate",
        reply: "Bye",
      };
    }
  });

  voxaApp.onIntent("NewGameIntent", voxaEvent => {
    if (voxaEvent.model.wins != undefined) {
      return {
        flow: "yield",
        reply: "StartNewGame",
        to: "shouldStartANewGame",
      };
    }
  });

  voxaApp.onIntent("HelpIntent", () => {
    return {
      flow: "continue",
      reply: "Help",
    };
  });

  voxaApp.onIntent("FallbackIntent", () => {
    return {
      flow: "continue",
      reply: "Fallback",
    };
  });
}

module.exports = register;
